{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Data.Semigroup ((<>))
import Options.Applicative
import Text.Read (readMaybe)

import Lib


data AppCmd = PrintHelp
            | DoFoo { _appCmd_s :: String }
            | DoBar { _appCmd_n :: Int } deriving (Show, Eq)

data AppOptions = LegacyOptions { _appOptions_dir :: String
                                , _appOptions_conc :: Int
                                , _appOptions_lim :: Maybe Int
                                }
                | AppOptions { _appOptions_cmd :: AppCmd
                             , _appOptions_global :: Bool
                             } deriving (Show, Eq)


fooParser = DoFoo <$> strOption (long "foos" <> help "Pass a string to foo")
barParser = DoBar <$> option auto (long "bari" <> help "Pass an integer to bar")
helpParser = pure PrintHelp

appCmdParser =
  subparser
  (  command "foo" (info (fooParser <**> helper) (progDesc "Does Foo"))
  <> command "bar" (info (barParser <**> helper) (progDesc "Does Bar"))
  <> command "elp"(info helpParser (progDesc "Prints HELP!")))

appOptionsParser :: Parser AppOptions
appOptionsParser = AppOptions
                   <$> appCmdParser
                   <*> switch
                       (  long "verbose"
                       <> short 'v'
                       <> showDefault
                       )

type ConcurrencyLevel = Int

concurrencyParser :: ReadM ConcurrencyLevel
concurrencyParser = eitherReader parse
  where
    parse :: String -> Either String Int
    parse s =
      case readMaybe s of
        Nothing -> Left $ "concurrency level is a number but: " ++ show s
        Just n | n < 1 ->
                 Left $ "concurrency level must be positive but: " ++ show n
               | otherwise -> Right n

legacyParser :: Parser AppOptions
legacyParser = LegacyOptions
               <$> argument str
                    (  metavar "DIR"
                    <> help "Path to folder with gzipped Hit archives and ppe.txt"
                    )
               <*> argument concurrencyParser
                    (  metavar "CONC"
                    <> help "concurrency level"
                    )
               <*> optional (argument auto
                    (  metavar "LIM"
                    <> help "Limit number of messages to replay from an archive"
                    ))


parseAppOptions :: IO AppOptions
parseAppOptions =
  execParser $ info ((appOptionsParser <|> legacyParser) <**> helper) desc
  where
    desc = progDesc "optparse abstract demo"

main :: IO ()
main = do
  ops <- parseAppOptions
  putStrLn $ "Got Options:\n" ++ show ops
  someFunc
